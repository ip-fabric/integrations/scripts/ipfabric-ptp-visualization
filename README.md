# IP Fabric PTP Visualization

## IP Fabric

IP Fabric is a vendor-neutral network assurance platform that automates the 
holistic discovery, verification, visualization, and documentation of 
large-scale enterprise networks, reducing the associated costs and required 
resources whilst improving security and efficiency.

It supports your engineering and operations teams, underpinning migration and 
transformation projects. IP Fabric will revolutionize how you approach network 
visibility and assurance, security assurance, automation, multi-cloud 
networking, and trouble resolution.

**Integrations or scripts should not be installed directly on the IP Fabric VM unless directly communicated from the
IP Fabric Support or Solution Architect teams.  Any action on the Command-Line Interface (CLI) using the root, osadmin,
or autoboss account may cause irreversible, detrimental changes to the product and can render the system unusable.**

## Project Description

< Summary of the Project >

## Installation

Install [Graphviz](https://graphviz.org/download/) and add location to your system path.

Install Python dependancies:

```bash
pip install -r requirements.txt

# Or using Poetry:

poetry install
```

## Configuration

The easiest way to use this package is with a `.env` file.  You can copy the sample and edit it with your environment variables. 

```commandline
cp sample.env .env
```

This contains the following variables which can also be set as environment variables instead of a .env file.
```
IPF_URL="https://demo3.ipfabric.io"
IPF_TOKEN=TOKEN
IPF_VERIFY=true
```

Or if using Username/Password:
```
IPF_URL="https://demo3.ipfabric.io"
IPF_USERNAME=USER
IPF_PASSWORD=PASS
```

## Usage

In [main.py](main.py) edit the following variables:

```python
from typing import List, Optional

OUT_DIR: Optional[str] = None  # Optional output directory for files

SITES: Optional[List[str]] = ["site15", "site11"]
DOMAIN: Optional[int] = 18
HOSTNAME: Optional[str] = "site15-rtr6"
```

- Optional:
  - OUT_DIR: str = Output directory to place SVG files if not in current working directory.
- Requires at least 1 of the following set:
  - SITES: List[str] = A list of site names in IP Fabric (case-sensitive).
  - DOMAIN: int = PTP domain number.
  - HOSTNAME: str = A hostname of a device in IP Fabric (case-sensitive).

### Running:

`python main.py`

## Support

TODO: How to open issue in Gitlab, some base information around it, etc.

## Contributing

TODO: How to contribute?
