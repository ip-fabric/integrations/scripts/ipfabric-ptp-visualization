from collections import defaultdict
from typing import Dict, List, Any, Optional
from pydantic import BaseModel, ConfigDict, PrivateAttr

from graphviz import Digraph

from ipf_ptp import LocalClock

NODE_TOOLTIP = {
    "Local Clock Identity": "local_clock_identity",
    "PTP Domain": "domain",
    "Local Clock Priority1": "local_priority_1",
    "Local Clock Priority2": "local_priority_2",
    "Steps Removed": "steps_removed",
    "Mode": "mode",
    "Local Clock Source IP": "source",
    "Mean Path Delay": "mean_path_delay",
    "Offset From Master": "offset_from_master",
    "Parent Clock Identity": "parent_clock_identity",
    "Parent Clock IP Address": "parent_ip_address",
    "Grandmaster Clock Identity": "grandmaster_clock_identity",
    "Grandmaster Clock Priority1": "grandmaster_priority_1",
    "Grandmaster Clock Priority2": "grandmaster_priority_2",
}


def create_node(node):
    shape = "ellipse"
    if node.local_is_grandmaster:
        shape = "house"
    elif not node.grandmaster_clock_identity:
        shape = "invhouse"
    return node.hostname, dict(
        tooltip="\n".join(
            [f"{k}: {getattr(node, v)}" for k, v in NODE_TOOLTIP.items()]
        ),
        shape=shape,
    )


class PTPGraph(BaseModel):
    model_config = ConfigDict(arbitrary_types_allowed=True)
    local_clocks: List[LocalClock]
    _graph: Digraph = PrivateAttr(None)
    _site_local: Optional[Dict[str, List[LocalClock]]] = PrivateAttr(None)
    _domain_local: Optional[List[LocalClock]] = PrivateAttr(None)
    _domain: Optional[int] = PrivateAttr(None)

    def _create_grandmaster_nodes(self, nodes: List[LocalClock]):
        grands = {
            (_.b64_grandmaster_clock_identity, _.grandmaster_clock_identity)
            for _ in nodes
            if _.local_is_grandmaster is False
        }
        with self._graph.subgraph(name="cluster_grandmasters") as c:
            c.attr(label="Grandmasters", tooltip="Grandmasters")
            for b, g in grands:
                c.node(b, label=g, shape="box", tooltip=g)

    def _add_transit(self, nodes):
        if True in nodes:
            self._graph.node("transit", shape="octagon", label="Transit")

    def _create_site_nodes(self):
        for site, nodes in self._site_local.items():
            with self._graph.subgraph(name=f"cluster_{site}") as c:
                c.attr(label=site, tooltip=site)
                for node in nodes:
                    name, params = create_node(node)
                    c.node(name, **params)
        self._add_transit({n.transit for _ in self._site_local.values() for n in _})

    def _create_domain_nodes(self):
        with self._graph.subgraph(name=f"cluster_{str(self._domain)}") as c:
            c.attr(
                label=f"Domain {str(self._domain)}",
                tooltip=f"Domain {str(self._domain)}",
            )
            for node in self._domain_local:
                name, params = create_node(node)
                c.node(name, **params)
        self._add_transit({_.transit for _ in self._domain_local})

    def _create_edges(self, clocks):
        transits = set()
        for _ in clocks:
            if _.parent and (
                self._domain
                or (not self._domain and _.parent.site_name in self._site_local)
            ):
                self._graph.edge(
                    _.parent.hostname,
                    _.hostname,
                    tooltip=f"{_.parent.hostname} -> {_.hostname}",
                )
            if _.transit:
                self._graph.edge(
                    "transit",
                    _.hostname,
                    tooltip=f"Unknown -> {_.hostname}",
                )
                transits.add(
                    (_.b64_grandmaster_clock_identity, _.grandmaster_clock_identity)
                )
            elif _.local_is_grandmaster is False and not _.parent:
                self._graph.edge(
                    _.b64_grandmaster_clock_identity,
                    _.hostname,
                    tooltip=f"{_.grandmaster_clock_identity} -> {_.hostname}",
                )
        for b, g in transits:
            self._graph.edge(b, "transit", tooltip=f"{g} -> Unknown")

    def _add_cloud_nodes_edges(self, site_clouds, site_edges):
        for site in site_clouds:
            self._graph.node(site, shape="octagon")
        for edge in site_edges:
            if edge[1] in self._site_local:
                continue
            self._graph.edge(
                edge[0],
                edge[1],
                tooltip=f"{edge[0]} -> {edge[1]}",
            )

    def _create_site_clouds(self):
        site_clouds, site_edges = set(), set()

        def parent_tree(clock: LocalClock):
            if not clock.parent:
                return
            if clock.parent.parent:
                parent_tree(clock.parent)
                if clock.parent.parent.site_name in self._site_local:
                    site_edges.add(
                        (clock.parent.parent.hostname, clock.parent.site_name)
                    )
                elif clock.parent.site_name not in self._site_local:
                    site_edges.add(
                        (clock.b64_grandmaster_clock_identity, clock.parent.site_name)
                    )
            if clock.parent.site_name not in self._site_local:
                site_clouds.add(clock.parent.site_name)
                if clock.site_name in self._site_local:
                    site_edges.add((clock.parent.site_name, clock.hostname))

        [parent_tree(n) for _ in self._site_local.values() for n in _]
        self._add_cloud_nodes_edges(site_clouds, site_edges)

    def graph_sites(self, sites: List[str]) -> Digraph:
        self._graph = Digraph("Site_PTP", format="svg")
        self._site_local, clocks = defaultdict(list), list()
        for _ in self.local_clocks:
            if _.site_name in sites:
                self._site_local[_.site_name].append(_)
                clocks.append(_)

        self._create_site_nodes()
        self._create_grandmaster_nodes(clocks)
        self._create_edges(clocks)
        self._create_site_clouds()
        self._site_local = None
        return self._graph

    def graph_domain(self, domain: int) -> Digraph:
        self._graph = Digraph("Domain_PTP", format="svg")
        self._domain = domain
        self._domain_local = [
            _ for _ in self.local_clocks if _.domain == domain
        ]
        self._create_domain_nodes()
        self._create_grandmaster_nodes(self._domain_local)
        self._create_edges(self._domain_local)
        self._domain, self._domain_local = None, None
        return self._graph


class PTPHostGraph(BaseModel):
    model_config = ConfigDict(arbitrary_types_allowed=True)
    local_clocks: List[LocalClock]
    _local_clock: Optional[LocalClock] = PrivateAttr(None)
    _graph: Digraph = PrivateAttr(None)

    def _create_grandmaster_nodes(self):
        if self._local_clock.local_is_grandmaster is not False:
            return
        with self._graph.subgraph(name="cluster_grandmasters") as c:
            c.attr(label="Grandmasters", tooltip="Grandmasters")
            c.node(
                self._local_clock.b64_grandmaster_clock_identity,
                label=self._local_clock.grandmaster_clock_identity,
                shape="box",
            )

    def _create_site_nodes(self):
        sites = defaultdict(set)

        def parent_nodes(clock: LocalClock):
            if clock.parent:
                parent_nodes(clock.parent)
            sites[clock.site_name].add(clock)
            if clock.transit:
                self._graph.node("transit", shape="octagon", label="Transit")
                self._graph.edge(
                    "transit",
                    clock.hostname,
                    tooltip=f"Unknown -> {clock.hostname}",
                )
                self._graph.edge(
                    clock.b64_grandmaster_clock_identity,
                    "transit",
                    tooltip=f"{clock.grandmaster_clock_identity} -> Unknown",
                )

        parent_nodes(self._local_clock)

        for site, nodes in sites.items():
            with self._graph.subgraph(name=f"cluster_{site}") as c:
                c.attr(label=site, tooltip=site)
                for node in nodes:
                    name, params = create_node(node)
                    c.node(name, **params)

    def _create_edges(self):

        def parent_edges(clock: LocalClock):
            if clock.parent:
                parent_edges(clock.parent)
                self._graph.edge(
                    clock.parent.hostname,
                    clock.hostname,
                    tooltip=f"{clock.parent.hostname} -> {clock.hostname}",
                )
            elif not clock.transit and clock.local_is_grandmaster is False:
                self._graph.edge(
                    clock.b64_grandmaster_clock_identity,
                    clock.hostname,
                    tooltip=f"{clock.grandmaster_clock_identity} -> {clock.hostname}",
                )

        parent_edges(self._local_clock)

    def graph(self, hostname: str) -> Digraph:
        self._graph = Digraph("Host_PTP", format="svg")
        self._local_clock = {_.hostname: _ for _ in self.local_clocks}[hostname]
        self._create_site_nodes()
        self._create_grandmaster_nodes()
        self._create_edges()
        self._local_clock = None
        return self._graph
