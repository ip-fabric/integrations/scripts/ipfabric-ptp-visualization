from base64 import b64encode
from typing import Optional, List

from pydantic import BaseModel, ConfigDict, Field


class Interface(BaseModel):
    state: str
    int_name: str = Field(alias="intName")


class LocalClock(BaseModel):
    model_config = ConfigDict(extra="ignore")
    sn: str
    hostname: str
    domain: int
    mode: str
    site_name: str = Field(alias="siteName")
    local_clock_identity: str = Field(alias="localClockIdentity")
    local_priority_1: Optional[int] = Field(None, alias="priority1")
    local_priority_2: Optional[int] = Field(None, alias="priority2")
    steps_removed: Optional[int] = Field(None, alias="stepsRemoved")
    source: Optional[str] = None
    mean_path_delay: Optional[int] = Field(None, alias="meanPathDelay")
    offset_from_master: Optional[int] = Field(None, alias="offsetFromMaster")
    parent: Optional["LocalClock"] = None
    interfaces: Optional[List[Interface]] = None
    grandmaster_priority_1: Optional[int] = Field(None, alias="grand_priority1")
    grandmaster_priority_2: Optional[int] = Field(None, alias="grand_priority2")
    parent_clock_identity: Optional[str] = Field(None, alias="parentClockIdentity")
    parent_ip_address: Optional[str] = Field(None, alias="parentIPAddress")
    grandmaster_clock_identity: Optional[str] = Field(
        None, alias="grandmasterClockIdentity"
    )

    def __hash__(self):
        return hash(self.local_clock_identity)

    @property
    def transit(self):
        if (
            not self.parent
            and self.parent_clock_identity != self.grandmaster_clock_identity
            and not self.local_is_parent
        ):
            return True
        return False

    @property
    def local_is_parent(self):
        return self.local_clock_identity == self.parent_clock_identity

    @property
    def local_is_grandmaster(self):
        if not self.grandmaster_clock_identity:
            return None
        return self.local_clock_identity == self.grandmaster_clock_identity

    @property
    def b64_grandmaster_clock_identity(self):
        return (
            b64encode(self.grandmaster_clock_identity.encode("utf-8")).decode("utf-8")
            if self.grandmaster_clock_identity
            else None
        )


def load_data(local_clocks_data: list, master_clocks: list):
    masters = dict()
    for _ in master_clocks:
        sn = _.pop("sn")
        [_.update({f"grand_{k}": _.pop(k)}) for k in ["priority1", "priority2"]]
        [_.pop(k, None) for k in ["hostname", "siteName", "id"]]
        [
            _.update({k: _[k].replace("0x", "") if _[k] else None})
            for k in ["parentClockIdentity", "grandmasterClockIdentity"]
        ]
        masters[sn] = _

    local_clocks = dict()
    for _ in local_clocks_data:
        _["localClockIdentity"] = _["localClockIdentity"].replace("0x", "")
        local_clocks[_["localClockIdentity"]] = (
            LocalClock(**_, **masters[_["sn"]])
            if _["sn"] in masters
            else LocalClock(**_)
        )

    for _ in local_clocks.values():
        if _.local_is_parent:
            continue
        elif _.parent_clock_identity in local_clocks:
            _.parent = local_clocks[_.parent_clock_identity]
        elif _.grandmaster_clock_identity in local_clocks:
            _.parent = local_clocks[_.grandmaster_clock_identity]

    return list(local_clocks.values())
