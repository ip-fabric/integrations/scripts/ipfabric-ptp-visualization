import json  # noqa
from typing import List, Optional

from ipfabric import IPFClient  # noqa

from graph import PTPGraph, PTPHostGraph
from ipf_ptp import load_data

OUT_DIR: Optional[str] = None  # Optional output directory for files

SITES: Optional[List[str]] = ["site15", "site11"]
DOMAIN: Optional[int] = 18
HOSTNAME: Optional[str] = "site15-rtr6"


if __name__ == "__main__":

    # Using exported JSON files from IP Fabric
    # with open("data/PTP_Local_clock.json", "r") as l:
    #     with open("data/PTP_Masters.json", "r") as m:
    #         local_clocks = load_data(json.load(l)["data"], json.load(m)["data"])

    # Using IPFabric SDK
    ipf = IPFClient()
    local_clocks = load_data(
        ipf.technology.management.ptp_local_clock.all(),
        ipf.technology.management.ptp_masters.all(),
    )

    if SITES:
        site_graph = PTPGraph(local_clocks=local_clocks)
        graph = site_graph.graph_sites(SITES)
        graph.render(filename="PTP_Sites", cleanup=True, directory=OUT_DIR)

    if DOMAIN:
        domain_graph = PTPGraph(local_clocks=local_clocks)
        graph = domain_graph.graph_domain(DOMAIN)
        graph.render(filename="PTP_Domain", cleanup=True, directory=OUT_DIR)

    if HOSTNAME:
        host_graph = PTPHostGraph(local_clocks=local_clocks)
        graph = host_graph.graph(HOSTNAME)
        graph.render(filename=f"PTP_{HOSTNAME}", cleanup=True, directory=OUT_DIR)
